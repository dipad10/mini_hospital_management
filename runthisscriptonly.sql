USE [master]
GO
/****** Object:  Database [Hms_Nicholas]    Script Date: 11/19/2018 3:19:29 PM ******/
CREATE DATABASE [Hms_Nicholas]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Hms_Nicholas', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Hms_Nicholas.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Hms_Nicholas_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Hms_Nicholas_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Hms_Nicholas] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Hms_Nicholas].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Hms_Nicholas] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET ARITHABORT OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Hms_Nicholas] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Hms_Nicholas] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Hms_Nicholas] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Hms_Nicholas] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET RECOVERY FULL 
GO
ALTER DATABASE [Hms_Nicholas] SET  MULTI_USER 
GO
ALTER DATABASE [Hms_Nicholas] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Hms_Nicholas] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Hms_Nicholas] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Hms_Nicholas] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Hms_Nicholas] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Hms_Nicholas', N'ON'
GO
USE [Hms_Nicholas]
GO
/****** Object:  Table [dbo].[Patients]    Script Date: 11/19/2018 3:19:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Patients](
	[PatientID] [int] IDENTITY(1000,1) NOT NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Address] [varchar](250) NULL,
	[Dateofbirth] [datetime] NULL,
	[Gender] [varchar](50) NULL,
	[Submittedon] [datetime] NULL,
	[Submittedby] [varchar](50) NULL,
	[Modifiedon] [datetime] NULL,
	[Modifiedby] [varchar](50) NULL,
 CONSTRAINT [PK_Patients] PRIMARY KEY CLUSTERED 
(
	[PatientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Payments]    Script Date: 11/19/2018 3:19:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Payments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Transactiondate] [datetime] NULL,
	[TransType] [varchar](255) NULL,
	[ReceiptNo] [varchar](255) NULL,
	[Narration] [varchar](1024) NULL,
	[BankAcctCode] [varchar](255) NULL,
	[BankAcctName] [varchar](255) NULL,
	[Bill] [money] NULL,
	[Amountpaid] [money] NULL,
	[Outstanding] [money] NULL,
	[Total] [money] NULL,
	[TransGUID] [varchar](255) NULL,
	[Deleted] [tinyint] NULL,
	[Active] [tinyint] NULL,
	[SubmittedBy] [varchar](255) NULL,
	[SubmittedOn] [datetime] NULL,
	[ModifiedBy] [varchar](255) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/19/2018 3:19:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(2000,1) NOT NULL,
	[Username] [varchar](255) NULL,
	[Password] [varchar](255) NULL,
	[Firstname] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Permission] [varchar](50) NULL,
	[Submittedon] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Patients] ON 

INSERT [dbo].[Patients] ([PatientID], [Firstname], [Lastname], [Address], [Dateofbirth], [Gender], [Submittedon], [Submittedby], [Modifiedon], [Modifiedby]) VALUES (1000, N'temitope', N'adegbesan', N'lilian street ikotun', CAST(N'2018-02-09 00:00:00.000' AS DateTime), N'male', CAST(N'2018-11-17 14:10:15.770' AS DateTime), N'ict', CAST(N'2018-11-17 15:30:10.767' AS DateTime), N'ict')
INSERT [dbo].[Patients] ([PatientID], [Firstname], [Lastname], [Address], [Dateofbirth], [Gender], [Submittedon], [Submittedby], [Modifiedon], [Modifiedby]) VALUES (1001, N'femi ', N'aderibigbe321', N'4 liliand sreette', CAST(N'1994-02-09 00:00:00.000' AS DateTime), N'male', CAST(N'2018-11-17 14:20:22.757' AS DateTime), N'ict', CAST(N'2018-11-17 15:41:09.180' AS DateTime), N'ict')
INSERT [dbo].[Patients] ([PatientID], [Firstname], [Lastname], [Address], [Dateofbirth], [Gender], [Submittedon], [Submittedby], [Modifiedon], [Modifiedby]) VALUES (1002, N'maculay', N'deji3233', N'4, lilijsdj jshddjshdj sds', CAST(N'1994-02-09 00:00:00.000' AS DateTime), N'female', CAST(N'2018-11-17 15:46:42.987' AS DateTime), N'ict', CAST(N'2018-11-17 15:47:40.193' AS DateTime), N'ict')
SET IDENTITY_INSERT [dbo].[Patients] OFF
SET IDENTITY_INSERT [dbo].[Payments] ON 

INSERT [dbo].[Payments] ([ID], [PatientID], [Firstname], [Lastname], [Transactiondate], [TransType], [ReceiptNo], [Narration], [BankAcctCode], [BankAcctName], [Bill], [Amountpaid], [Outstanding], [Total], [TransGUID], [Deleted], [Active], [SubmittedBy], [SubmittedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1000, N'temitope', N'adegbesan', CAST(N'2018-11-19 00:00:00.000' AS DateTime), N'CASH', N'4545544545', N'dfdffdfd', N'45444554', N'Ecobank', 5000.0000, 5000.0000, 0.0000, 5000.0000, N'3b07853b-08aa-404f-92ee-2b46b8cba0da', 0, 1, N'ict', CAST(N'2018-11-19 10:26:57.747' AS DateTime), N'ict', CAST(N'2018-11-19 13:00:52.003' AS DateTime))
SET IDENTITY_INSERT [dbo].[Payments] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserID], [Username], [Password], [Firstname], [Lastname], [Permission], [Submittedon]) VALUES (2001, N'ict', N'mamush', N'ola', N'ade', N'admin', CAST(N'2018-02-02 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Users] OFF
USE [master]
GO
ALTER DATABASE [Hms_Nicholas] SET  READ_WRITE 
GO
