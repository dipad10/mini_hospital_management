﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Hospital_management.Models
{
    public class Payments
    {
        public int ID { get; set; }
        [Required]
        public int PatientID { get; set; }
       
        public string Firstname { get; set; }
       
        public string Lastname { get; set; }
        [Required]
        public DateTime? Transactiondate { get; set; }
        [Required]
        public string Transtype { get; set; }
        [Required]

        public string Receiptno { get; set; }
        public string Narration { get; set; }
        [Required]
        public decimal Bill { get; set; }
        public string Bankacctcode { get; set; }

        public string Bankacctname { get; set; }

        public decimal Amountpaid { get; set; }
        public decimal Outstanding { get; set; }
        public decimal Total { get; set; }
       
        public DateTime? Submittedon { get; set; }
       
       
    }
}