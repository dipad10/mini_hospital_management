﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Hospital_management.Models
{
    public class Patients
    {
        public  int PatientID { get; set; }
        [Required]
        public  string Firstname { get; set; }
        [Required]
        public string Lastname { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public DateTime? Dateofbirth { get; set; }
        [Required]
        public string Gender { get; set; }

        public DateTime? Submittedon { get; set; }

        public string Submittedby { get; set; }
        [NotMapped]
        public string Fullnameandid { get; set; }
     
    }

  
}