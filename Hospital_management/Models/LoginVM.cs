﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hospital_management.Models
{
    public class LoginVM
    {
        public int ID { get; set; }
        [Required]
        public string Username { get; set; }
        [Required] 
        public string Password { get; set; }
    }
}