﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Hospital_management.Models;

namespace Hospital_management.DAL
{
    public class Cls_payments
    {

        string strConString = System.Configuration.ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString;
        /// <summary>  
        /// Get all records from the DB  
        /// </summary>  
        /// <returns>Datatable</returns>  
        public IEnumerable<Payments> GetAllPayments()
        {
            List<Payments> lstpayments = new List<Payments>();

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from payments order by ID desc", con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {

                    Payments payment = new Payments();
                    payment.ID = Convert.ToInt32(rdr["id"]);

                    payment.PatientID = Convert.ToInt32(rdr["patientid"]);

                    payment.Firstname = rdr["firstname"].ToString();
                    payment.Lastname = rdr["lastname"].ToString();

                    payment.Transactiondate = Convert.ToDateTime(rdr["transactiondate"]);

                    payment.Transtype = rdr["transtype"].ToString();

                    payment.Receiptno = rdr["receiptno"].ToString();
                    payment.Narration = rdr["narration"].ToString();
                    payment.Bankacctcode = rdr["bankacctcode"].ToString();
                    payment.Bankacctname = rdr["bankacctname"].ToString();
                    payment.Amountpaid = Convert.ToDecimal(rdr["amountpaid"]);
                    payment.Bill = Convert.ToDecimal(rdr["bill"]);
                    payment.Outstanding = Convert.ToDecimal(rdr["outstanding"]);
                    payment.Total = Convert.ToDecimal(rdr["total"]);
                  
                    payment.Narration = rdr["narration"].ToString();
                    payment.Submittedon = Convert.ToDateTime(rdr["submittedon"]);

                    lstpayments.Add(payment);

                }

                con.Close();
            }
            return lstpayments;
        }
        public Payments GetPaymentByID(int? id)
        {
            Payments payment = new Payments();



            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from payments where id=" + id, con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {


                    payment.ID = Convert.ToInt32(rdr["id"]);

                    payment.PatientID = Convert.ToInt32(rdr["patientid"]);

                    payment.Firstname = rdr["firstname"].ToString();
                    payment.Lastname = rdr["lastname"].ToString();

                    payment.Transactiondate = Convert.ToDateTime(rdr["transactiondate"]);

                    payment.Transtype = rdr["transtype"].ToString();

                    payment.Receiptno = rdr["receiptno"].ToString();
                    payment.Narration = rdr["narration"].ToString();
                    payment.Bankacctcode = rdr["bankacctcode"].ToString();
                    payment.Bankacctname = rdr["bankacctname"].ToString();
                    payment.Amountpaid = Convert.ToDecimal(rdr["amountpaid"]);
                    payment.Bill = Convert.ToDecimal(rdr["bill"]);
                    payment.Outstanding = Convert.ToDecimal(rdr["outstanding"]);
                    payment.Total = Convert.ToDecimal(rdr["total"]);
                   
                    payment.Narration = rdr["narration"].ToString();
                    payment.Submittedon = Convert.ToDateTime(rdr["submittedon"]);



                }

                con.Close();
            }
            return payment;
        }

        public List<Payments> GetPaymentBypatientID(int? patientid)
        {
            List<Payments> lspayments = new List<Payments>();



            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from payments where patientid=" + patientid, con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {
                    Payments payment = new Payments();

                    payment.ID = Convert.ToInt32(rdr["id"]);

                    payment.PatientID = Convert.ToInt32(rdr["patientid"]);

                    payment.Firstname = rdr["firstname"].ToString();
                    payment.Lastname = rdr["lastname"].ToString();

                    payment.Transactiondate = Convert.ToDateTime(rdr["transactiondate"]);

                    payment.Transtype = rdr["transtype"].ToString();

                    payment.Receiptno = rdr["receiptno"].ToString();
                    payment.Narration = rdr["narration"].ToString();
                    payment.Bankacctcode = rdr["bankacctcode"].ToString();
                    payment.Bankacctname = rdr["bankacctname"].ToString();
                    payment.Amountpaid = Convert.ToDecimal(rdr["amountpaid"]);
                    payment.Bill = Convert.ToDecimal(rdr["bill"]);
                    payment.Outstanding = Convert.ToDecimal(rdr["outstanding"]);
                    payment.Total = Convert.ToDecimal(rdr["total"]);
                   
                    payment.Narration = rdr["narration"].ToString();
                    payment.Submittedon = Convert.ToDateTime(rdr["submittedon"]);

                    lspayments.Add(payment);

                }

                con.Close();
            }
            return lspayments;
        }

        /// <returns></returns>  
        public int Updatepayment(Payments payment)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Update payments SET Patientid=@patientid, firstname=@firstname, lastname=@lastname, transactiondate=@transactiondate, transtype=@transtype, receiptno=@receiptno, narration=@narration, bankacctcode=@bankacctcode, bankacctname=@bankacctname, bill=@bill, amountpaid=@amountpaid, outstanding=@outstanding, total=@total, modifiedby=@modifiedby, modifiedon=@modifiedon where id=@id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@id", payment.ID);
                cmd.Parameters.AddWithValue("@patientid", payment.PatientID);
                cmd.Parameters.AddWithValue("@firstname", payment.Firstname);
                cmd.Parameters.AddWithValue("@lastname", payment.Lastname);
                cmd.Parameters.AddWithValue("@transactiondate", payment.Transactiondate);
                cmd.Parameters.AddWithValue("@transtype", payment.Transtype);
                cmd.Parameters.AddWithValue("@receiptno", payment.Receiptno);
                cmd.Parameters.AddWithValue("@narration", payment.Narration);
                cmd.Parameters.AddWithValue("@bankacctcode", payment.Bankacctcode);
                cmd.Parameters.AddWithValue("@bankacctname", payment.Bankacctname);
                cmd.Parameters.AddWithValue("@bill", payment.Bill);
                cmd.Parameters.AddWithValue("@amountpaid", payment.Amountpaid);
                cmd.Parameters.AddWithValue("@outstanding", payment.Outstanding);
                cmd.Parameters.AddWithValue("@total", payment.Total);
                cmd.Parameters.AddWithValue("@modifiedon", DateTime.Now);
                cmd.Parameters.AddWithValue("@modifiedby", HttpContext.Current.Session["Username"]);

                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>  
        /// Insert user record into DB  
        /// </summary>  

        /// <returns></returns>  
        public int Insertpayment(Payments payment)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Insert into payments (Patientid, firstname, lastname, transactiondate, transtype, receiptno, narration, bankacctcode, bankacctname, bill, amountpaid, outstanding, total, transguid, deleted, active, submittedby, submittedon) values(@Patientid, @firstname, @lastname, @transactiondate, @transtype, @receiptno, @narration, @bankacctcode, @bankacctname, @bill, @amountpaid, @outstanding, @total, @transguid, @deleted, @active, @submittedby, @submittedon)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@Patientid", payment.PatientID);
                cmd.Parameters.AddWithValue("@firstname", payment.Firstname);
                cmd.Parameters.AddWithValue("@lastname", payment.Lastname);
                cmd.Parameters.AddWithValue("@transactiondate", payment.Transactiondate);
                cmd.Parameters.AddWithValue("@transtype", payment.Transtype);
                cmd.Parameters.AddWithValue("@receiptno", payment.Receiptno);
                cmd.Parameters.AddWithValue("@narration", payment.Narration);
                cmd.Parameters.AddWithValue("@bankacctcode", payment.Bankacctcode);
                cmd.Parameters.AddWithValue("@bankacctname", payment.Bankacctname);
                cmd.Parameters.AddWithValue("@bill", payment.Bill);
                cmd.Parameters.AddWithValue("@amountpaid", payment.Amountpaid);
                cmd.Parameters.AddWithValue("@outstanding", payment.Outstanding);
                cmd.Parameters.AddWithValue("@total", payment.Total);
                cmd.Parameters.AddWithValue("@transguid", Guid.NewGuid().ToString());
                
                cmd.Parameters.AddWithValue("@deleted", 0);
                cmd.Parameters.AddWithValue("@active", 1);
                cmd.Parameters.AddWithValue("@submittedon", DateTime.Now);
                cmd.Parameters.AddWithValue("@submittedby", HttpContext.Current.Session["Username"]);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Deletepayment(int id)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Delete from payments where id=@id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@id", id);
                return cmd.ExecuteNonQuery();
            }
        }

        public IEnumerable<Payments> GetPaymentlistByID(int? id)
        {
            List<Payments> lstpayment = new List<Payments>();



            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from payments where id=" + id, con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {

                    Payments payment = new Payments();
                    payment.ID = Convert.ToInt32(rdr["id"]);

                    payment.PatientID = Convert.ToInt32(rdr["patientid"]);

                    payment.Firstname = rdr["firstname"].ToString();
                    payment.Lastname = rdr["lastname"].ToString();

                    payment.Transactiondate = Convert.ToDateTime(rdr["transactiondate"]);

                    payment.Transtype = rdr["transtype"].ToString();

                    payment.Receiptno = rdr["receiptno"].ToString();
                    payment.Narration = rdr["narration"].ToString();
                    payment.Bankacctcode = rdr["bankacctcode"].ToString();
                    payment.Bankacctname = rdr["bankacctname"].ToString();
                    payment.Amountpaid = Convert.ToDecimal(rdr["amountpaid"]);
                    payment.Bill = Convert.ToDecimal(rdr["bill"]);
                    payment.Outstanding = Convert.ToDecimal(rdr["outstanding"]);
                    payment.Total = Convert.ToDecimal(rdr["total"]);

                    payment.Narration = rdr["narration"].ToString();
                    payment.Submittedon = Convert.ToDateTime(rdr["submittedon"]);
                    lstpayment.Add(payment);


                }

                con.Close();
            }
            return lstpayment;
        }
    }
}
