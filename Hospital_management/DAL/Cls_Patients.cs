﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Hospital_management.Models;


namespace Hospital_management.DAL
{
    public class Cls_Patients
    {
        string strConString = System.Configuration.ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString;
        /// <summary>  
        /// Get all records from the DB  
        /// </summary>  
        /// <returns>Datatable</returns>  
        public IEnumerable<Patients> GetAllPatients()
        {
            List<Patients> lstpatients = new List<Patients>();

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Patients", con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {

                    Patients patient = new Patients();

                    patient.PatientID = Convert.ToInt32(rdr["patientid"]);

                    patient.Firstname = rdr["firstname"].ToString();
                    patient.Lastname = rdr["lastname"].ToString();

                    patient.Address = rdr["address"].ToString();

                    patient.Dateofbirth = Convert.ToDateTime(rdr["dateofbirth"]);

                    patient.Gender = rdr["gender"].ToString();
                    
                    patient.Submittedon = Convert.ToDateTime(rdr["submittedon"]);
                    patient.Fullnameandid = string.Format("{0}, {1} {2}", Convert.ToInt32(rdr["patientid"]), rdr["firstname"].ToString(), rdr["lastname"].ToString());
                    lstpatients.Add(patient);

                }

                con.Close();
            }
            return lstpatients;
        }

        public List<Patients> Selectall()
        {
            List<Patients> lstpatients = new List<Patients>();

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Patients order by patientid desc", con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {

                    Patients patient = new Patients();

                    patient.PatientID = Convert.ToInt32(rdr["patientid"]);

                    patient.Firstname = rdr["firstname"].ToString();
                    patient.Lastname = rdr["lastname"].ToString();

                    patient.Address = rdr["address"].ToString();

                    patient.Dateofbirth = Convert.ToDateTime(rdr["dateofbirth"]);

                    patient.Gender = rdr["gender"].ToString();

                    patient.Submittedon = Convert.ToDateTime(rdr["submittedon"]);
                    patient.Fullnameandid = string.Format("{0}, {1} {2}", Convert.ToInt32(rdr["patientid"]), rdr["firstname"].ToString(), rdr["lastname"].ToString());
                    lstpatients.Add(patient);

                }

                con.Close();
            }
            return lstpatients;
        }
        public Patients GetPatientByID(int? patientid)
        {
            Patients patient = new Patients();



            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from patients where patientid=" + patientid, con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {

                  
                    patient.PatientID = Convert.ToInt32(rdr["patientid"]);

                    patient.Firstname = rdr["firstname"].ToString();
                    patient.Lastname = rdr["lastname"].ToString();

                    patient.Address = rdr["address"].ToString();

                    patient.Dateofbirth = Convert.ToDateTime(rdr["dateofbirth"]);

                    patient.Gender = rdr["gender"].ToString();

                    patient.Submittedon = Convert.ToDateTime(rdr["submittedon"]);

                    

                }

                con.Close();
            }
            return patient;
        }


        /// <returns></returns>  
        public int Updatepatients(Patients patient)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Update patients SET Firstname=@firstname, lastname=@lastname, address=@address, dateofbirth=@dateofbirth, gender=@gender, modifiedon=@modifiedon, modifiedby=@modifiedby where patientid=@patientid";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@patientid", patient.PatientID);
                cmd.Parameters.AddWithValue("@firstname", patient.Firstname);
                cmd.Parameters.AddWithValue("@lastname", patient.Lastname);
                cmd.Parameters.AddWithValue("@address", patient.Address);
                cmd.Parameters.AddWithValue("@dateofbirth", patient.Dateofbirth);
                cmd.Parameters.AddWithValue("@gender", patient.Gender);
                cmd.Parameters.AddWithValue("@modifiedon", DateTime.Now);
                cmd.Parameters.AddWithValue("@modifiedby", HttpContext.Current.Session["Username"]);

                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>  
        /// Insert user record into DB  
        /// </summary>  

        /// <returns></returns>  
        public int Insertpatient(Patients patient)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Insert into Patients (firstname, lastname, address, dateofbirth, gender, submittedon, submittedby) values(@firstname, @lastname , @address, @dateofbirth, @gender, @submittedon, @submittedby)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@firstname", patient.Firstname);
                cmd.Parameters.AddWithValue("@lastname", patient.Lastname);
                cmd.Parameters.AddWithValue("@address", patient.Address);
                cmd.Parameters.AddWithValue("@dateofbirth", patient.Dateofbirth);
                cmd.Parameters.AddWithValue("@gender", patient.Gender);
                cmd.Parameters.AddWithValue("@submittedon", DateTime.Now);
                cmd.Parameters.AddWithValue("@submittedby", HttpContext.Current.Session["Username"]);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Deletepatient(int patientid)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Delete from patients where patientid=@patientid";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@patientid", patientid);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
