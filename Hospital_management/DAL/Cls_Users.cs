﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Hospital_management.Models;
using System.Configuration;
namespace Hospital_management.DAL
{
    public class Cls_Users
    {
        string strConString = System.Configuration.ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString;
        /// <summary>  
        /// Get all records from the DB  
        /// </summary>  
        /// <returns>Datatable</returns>  
        public IEnumerable<Users> GetAllUsers()
        {
            List<Users> lstusers = new List<Users>();
         
            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from Users order by submittedon desc", con)
                {
                    CommandType = CommandType.Text
                };
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {

                    Users user = new Users();

                    user.ID = Convert.ToInt32(rdr["userid"]);

                    user.Username = rdr["username"].ToString();

                    user.Firstname = rdr["firstname"].ToString();

                    user.Lastname = rdr["lastname"].ToString();

                    user.Permission = rdr["permission"].ToString();
                    user.Submittedon = Convert.ToDateTime(rdr["submittedon"]);
                   
                    lstusers.Add(user);

                }

                con.Close();
            }
            return lstusers;
        }
        public Users GetuserByID(int? userid)
        {
            Users user = new Users();
            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * from users where userid=" + userid, con);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())

                {

                   

                    user.ID = Convert.ToInt32(rdr["userid"]);

                    user.Username = rdr["username"].ToString();

                    user.Firstname = rdr["firstname"].ToString();

                    user.Lastname = rdr["lastname"].ToString();

                    user.Permission = rdr["permission"].ToString();
                    user.Submittedon = Convert.ToDateTime(rdr["submittedon"]);
                    
                }

                con.Close();
            }
            return user;
        }

      
        /// <returns></returns>  
        public int Updateuser(Users user)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Update users SET username=@username, firstname=@firstname, lastname=@lastname, password=@password, permission=@permission where userid=@userid";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", user.Username);
                cmd.Parameters.AddWithValue("@firstname", user.Firstname);
                cmd.Parameters.AddWithValue("@lastname", user.Lastname);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@permission", user.Permission);
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>  
        /// Insert user record into DB  
        /// </summary>  
     
        /// <returns></returns>  
        public int Insertuser(Users user)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Insert into Users (username, firstname, lastname, password, permission, submittedon) values(@username, @firstname , @lastname, @password, @permission, @submittedon)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", user.Username);
                cmd.Parameters.AddWithValue("@firstname", user.Firstname);
                cmd.Parameters.AddWithValue("@lastname", user.Lastname);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@permission", user.Permission);
                cmd.Parameters.AddWithValue("@submittedon", DateTime.Now);
                return cmd.ExecuteNonQuery();
            }
        }

        public int Deleteuser(int userid)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "Delete from users where userid=@userid";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@userid", userid);
                return cmd.ExecuteNonQuery();
            }
        }


        public bool AuthenticateLogin(LoginVM login)
        {

            using (SqlConnection con = new SqlConnection(strConString))
            {
                con.Open();
                string query = "select * from users where username=@username and password=@password";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", login.Username);
                cmd.Parameters.AddWithValue("@password", login.Password);
               cmd.ExecuteNonQuery();
                SqlDataReader dr;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    //valid user
                    return true;
                }
                else
                {
                    //invalid user
                    return false;
                }
            }
        }
    }
}
