﻿using Hospital_management.DAL;
using Hospital_management.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Hospital_management.Controllers
{
   
    public class PaymentsController : Controller
    {
        // GET: Payments
        public ActionResult Index()
        {
            var payments = new Cls_payments().GetAllPayments();
            return View(payments);
        }

        public ActionResult Payments_edit(int? id)
         {
            //if id is available thenget data back to user else retrun new model
            if (id != null)
            {
                var payment = new Cls_payments().GetPaymentByID(id);
                List<Patients> list = (new Cls_Patients()).Selectall();
                ViewBag.patientlist = new SelectList(list, "PatientID", "Fullnameandid");
                return View(payment);
            }
            else
            {
                Payments payment = new Payments();
                List<Patients> list = (new Cls_Patients()).Selectall();
                ViewBag.patientlist = new SelectList(list, "PatientID", "Fullnameandid");

                return View(payment);
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Payments_edit(Payments payment)
        {

            if (ModelState.IsValid)
            {
                int result = 0;
                //get patient names by id posted
                Patients patient = (new Cls_Patients()).GetPatientByID(payment.PatientID);

                //check if exist
                if (payment.ID > 0)
                {
                    //do update
                    payment.Firstname = patient.Firstname;
                    payment.Lastname = patient.Lastname;
                    result = (new Cls_payments()).Updatepayment(payment);
                    if (result == 1)
                    {
                        //success
                        TempData["success"] = "true";
                        TempData["message"] = "Payment updated successfully";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                        TempData["error"] = "false";
                        TempData["message"] = "An error occured";
                        return View(payment);
                    }
                }
                else
                {
                    //do insert
                    payment.Firstname = patient.Firstname;
                    payment.Lastname = patient.Lastname;
                    result = (new Cls_payments()).Insertpayment(payment);
                    if (result == 1)
                    {
                        //success
                        TempData["success"] = "true";
                        TempData["message"] = "Payment added successfully";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                        TempData["error"] = "false";
                        TempData["message"] = "An error occured";
                        return View(payment);
                    }
                }




            }
            List<Patients> list = (new Cls_Patients()).Selectall();
            ViewBag.patientlist = new SelectList(list, "PatientID", "Fullnameandid");
            return View(payment);
        }

        public ActionResult deletepayment(int id)
        {
            int result = 0;
            //check if payment is available
            var payment = (new Cls_payments()).GetPaymentByID(id);
            if (payment != null)
            {
                result = (new Cls_payments()).Deletepayment(id);
                TempData["success"] = "true";
                TempData["message"] = "Payment deleted successfully";
                return RedirectToAction("Index");
            }


            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


        }

        public ActionResult Getpayment(int id)
        {
            var payment = (new Cls_payments()).GetPaymentlistByID(id);
            return View(payment);
        }

        public ActionResult print(int id)
        {
            var q = new ActionAsPdf("Getpayment", new { id = id });
            
            return q;

        }
    }
}