﻿using Hospital_management.DAL;
using Hospital_management.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Hospital_management.Controllers
{
    [Filters.Authorizeusers]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Users()
        {
           var users = (new Cls_Users()).GetAllUsers();
            return View(users);
        }

        public ActionResult Users_edit(int? id)
        {
            //if id is available thenget data back to user else retrun new model
            if (id != null)
            {
                var user = (new Cls_Users()).GetuserByID(id);
                
                return View(user);
            }
            else
            {
                Users user = new Users();
                return View(user);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Users_edit(Users user)
        {

            if (ModelState.IsValid)
            {
                int result = 0;
                //check if exist
                if (user.ID > 0)
                {
                    //do update

                    result = (new Cls_Users()).Updateuser(user);
                    if (result == 1)
                    {
                        //success
                        TempData["success"] = "true";
                        TempData["message"] = "Data updated successfully";
                        return RedirectToAction("Users");
                    }
                    else
                    {
                        //error
                        TempData["error"] = "false";
                        TempData["message"] = "An error occured";
                        return View(user);
                    }
                }
                else
                {
                    //do insert
                    result = (new Cls_Users()).Insertuser(user);
                    if (result == 1)
                    {
                        //success
                        TempData["success"] = "true";
                        TempData["message"] = "Data saved successfully";
                        return RedirectToAction("Users");
                    }
                    else
                    {
                        //error
                        TempData["error"] = "false";
                        TempData["message"] = "An error occured";
                        return View(user);
                    }
                }




            }
            return View(user);
        }
        
        public ActionResult deleteuser(int id)
        {
            int result = 0;
            //check if user is available
            var user = (new Cls_Users()).GetuserByID(id);
            if(user != null)
            {
                result = (new Cls_Users()).Deleteuser(id);
                TempData["success"] = "true";
                TempData["message"] = "user deleted successfully";
                return RedirectToAction("Users");
            }


            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


        }
    }
}