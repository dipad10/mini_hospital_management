﻿using Hospital_management.DAL;
using Hospital_management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hospital_management.Controllers
{
 
    public class HomeController : Controller
    {
        public ActionResult Login()
        {
            Session.Clear();
            Session.Abandon();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVM vm)
        {
            if(ModelState.IsValid)
            {
                bool isuservalid = (new Cls_Users()).AuthenticateLogin(vm);
                if(isuservalid)
                {
                    Session["Username"] = vm.Username;
                    return View("Dashboard");
                }
                else
                {
                    //invaliduser
                    ViewBag.invalid = "Invalid username or password";
                    return View(vm);
                }

            }
            return View(vm);
           
        }

        public ActionResult Dashboard()
        {
           
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Login");
        }

    }
}