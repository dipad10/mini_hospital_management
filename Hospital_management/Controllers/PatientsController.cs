﻿using Hospital_management.DAL;
using Hospital_management.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hospital_management.Controllers
{
    [Filters.Authorizeusers]
    public class PatientsController : Controller
    {
        // GET: Patients
        public ActionResult Index()
        {
            var patients = (new Cls_Patients()).GetAllPatients();
            return View(patients);
        }

        public ActionResult Patient_edit(int? id)
        {
            //if id is available thenget data back to user else retrun new model
            if(id != null)
            {
                var patient = (new Cls_Patients()).GetPatientByID(id);
                return View(patient);
            }
            else
            {
                Patients patient = new Patients();
                return View(patient);
            }
           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Patient_edit(Patients patient)
        {
            
            if(ModelState.IsValid)
            {
                int result = 0;
                //check if exist
                if (patient.PatientID > 0)
                {
                    //do update
                    
                    result = (new Cls_Patients()).Updatepatients(patient);
                    if (result == 1)
                    {
                        //success
                        TempData["success"] = "true";
                        TempData["message"] = "Patient updated successfully";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                        TempData["error"] = "false";
                        TempData["message"] = "An error occured";
                        return View(patient);
                    }
                }
                else
                {
                    //do insert
                    result = (new Cls_Patients()).Insertpatient(patient);
                    if (result == 1)
                    {
                        //success
                        TempData["success"] = "true";
                        TempData["message"] = "Patient added successfully";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //error
                        TempData["error"] = "false";
                        TempData["message"] = "An error occured";
                        return View(patient);
                    }
                }
               
                    
                 
               
            }
            return View(patient);
        }
    }
}